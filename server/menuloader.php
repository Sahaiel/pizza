<?php
header("Access-Control-Allow-Origin: http://localhost:3000");
require_once 'db.php';
mysqli_report(MYSQLI_REPORT_STRICT); //wyłącza wyświetlanie kodów o błędach
try
{
    $connect_db = new mysqli($db_host, $db_login, $db_password, $db_name);
    if($connect_db->connect_errno!=0)
        throw new Exception(mysqli_connect_errno());
    else
    {
        //echo $type;
        $response = $connect_db->query("SELECT * FROM menu");
        if(!$response) throw new Exception($connect_db->error); //rzuca nowy kod błedu wynikający ze złego przesłania kwerendy
        $menu = $response->fetch_all(MYSQLI_ASSOC);
        //echo "<pre>";
        print_r(json_encode($menu));
       // echo "</pre>";
        //echo $_GET['type'];
    }
    $connect_db->close();
}
catch(Exception $e) //wyjatek
{
    echo 'Błąd serwera. ';
    echo 'Informacja developerska: '.$e; //Informacja dla dev
	
}

?>