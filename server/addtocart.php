<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
print_r($_POST);

if(isset($_POST['dish_id']) && isset($_POST['size']))
{
    $dish_id = $_POST['dish_id'];
    $user_id = 1;//$POST['user_id'];
    $size = $_POST['size'];
    if($size == "small" || $size == "medium" || $size == "big")
    {
        require_once 'db.php';
        mysqli_report(MYSQLI_REPORT_STRICT); //wyłącza wyświetlanie kodów o błędach
        try
        {
            $connect_db = new mysqli($db_host, $db_login, $db_password, $db_name);
            if($connect_db->connect_errno!=0)
                throw new Exception(mysqli_connect_errno());
            else
            {
                $response = $connect_db->query("INSERT INTO cart VALUES (null, {$dish_id}, {$user_id}, '$size')");
                if(!$response) throw new Exception($connect_db->error); //rzuca nowy kod błedu wynikający ze złego przesłania kwerendy
            }
            $connect_db->close();
        }
        catch(Exception $e) //wyjatek
        {
            echo 'Błąd serwera. ';
            echo 'Informacja developerska: '.$e; //Informacja dla dev
            
        }
    }   
}
else 
{
    echo 'nie działa';
}
?>