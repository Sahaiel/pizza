<?php
header("Access-Control-Allow-Origin: http://localhost:3000");
require_once 'db.php';
mysqli_report(MYSQLI_REPORT_STRICT); //wyłącza wyświetlanie kodów o błędach
try
{   
    $user_id = 1;//tu będzie $_SESSION['user_id'];
    $connect_db = new mysqli($db_host, $db_login, $db_password, $db_name);
    if($connect_db->connect_errno!=0)
        throw new Exception(mysqli_connect_errno());
    else
    {
    $response = $connect_db->query("SELECT m.name, m.description, m.small, m.medium, m.big, c.size, c.id FROM menu m left join cart c on m.id = c.dish_id WHERE c.dish_id IS NOT null AND c.user_id = {$user_id}");
        if(!$response) throw new Exception($connect_db->error); //rzuca nowy kod błedu wynikający ze złego przesłania kwerendy
        $cart = $response->fetch_all(MYSQLI_ASSOC);

        print_r(json_encode($cart));
    }
    $connect_db->close();
}
catch(Exception $e) //wyjatek
{
		echo 'Błąd serwera. ';
		echo 'Informacja developerska: '.$e; //Informacja dla dev
	
}

?>