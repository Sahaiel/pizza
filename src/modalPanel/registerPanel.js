import React, { Component } from 'react';
import { Link } from 'react-router-dom'



class registerPanel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: ""
		};
		//this.changeValue = this.changeValue.bind(this)//wrazie error usunąc
	}

	changeValue(event){
		this.setState({
				text: "^"+event.target.value+"$"
		});
	}
  	render() {
		return (
		<div className="row">
		
				<div className="col-lg-12">
					<h2>Panel rejestracji</h2>
					<form>
							<div className="form-group">
							<label>Adres email</label>
							<input type="email" name="email" className="form-control" placeholder="Wpisz email" required />
							</div>
							<div className="form-group">
							<label>Miasto i kod pocztowy</label>
							<input type="text" name="city" className="form-control" placeholder="Miasto, 12-345" required/>
							</div>
							<div className="form-group">
							<label>Ulica i numer domu</label>
							<input type="text" name="adress" className="form-control" placeholder="Ulica 13/4" required/>
							</div>
							<div className="form-group">
							<label>Telefon</label>
							<input type="tel" name="tel" className="form-control" placeholder="tel." required/>
							</div>
							<div className="form-group">
							<label htmlFor="inputPassword">Hasło</label>
							<input type="password" className="form-control" id="inputPassword" pattern="^[a-zA-Z0-9]{6,}$" onChange={this.changeValue} placeholder="min. 6 znaków, bez znaków specjalnych" required />
							</div>
							<div className="form-group">
							<label htmlFor="confirmPassword">Powtórz hasło</label>
							<input type="password" className="form-control" placeholder="Powtórz hasło" pattern={this.state.text} required />
							</div>
							<button type="submit" className="btn btn-primary">Zarejestruj</button>&nbsp;
							<Link to='/'><button type="button" className="btn btn-primary">Panel logowania</button></Link>
					</form>
				</div>

			</div>
		);
	}
}

export default registerPanel;