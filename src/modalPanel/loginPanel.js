import React, { Component } from 'react';
import { Link } from 'react-router-dom'


class Panel extends Component {
  render() {
    return (
			
    <div className="row">

			<div className="col-lg-12">
				<h2>Panel logowania</h2>
				<form>
						<div className="form-group">
						<label htmlFor="exampleInputEmail1">Adres email</label>
						<input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Wpisz email" />
						</div>
						<div className="form-group">
						<label htmlFor="exampleInputPassword1">Hasło</label>
						<input type="password" className="form-control" id="exampleInputPassword1" placeholder="Podaj hasło" />
						</div>
						<button type="submit" className="btn btn-primary">Zaloguj</button>&nbsp;
						<Link to='/register'><button type="button" className="btn btn-primary">Panel rejestracji</button></Link>
						
				</form>
			</div>
		

		</div>
    );
  }
}

export default Panel;