import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import qs from 'qs'
var host = "http://localhost/";

class Cart extends Component {
	constructor(props) {
		super(props)

		this.state = {
			tabela: [],
			cart: []
		}
	}

	componentDidMount() {
		this.loadCart()
	}

	loadCart()
	{
		//axios.post('../server/cartloader.php')
		axios.post(host + 'pizza/server/cartloader.php')
		.then( response => {
			console.log('Ajax cart works');
			//console.log(response.data);
			this.setState( (state) => ({tabela: response.data}));

			let cart = this.makeTable(this.state.tabela);
			this.setState( (state) => ({cart: cart}));
		}); 
	}

	removeFromCart(id)
	{
		axios.post(host + 'pizza/server/removefromcart.php', qs.stringify({'id': id}))
		.then( response => {
			console.log('Ajax cart works');
			//console.log(response.data);
			console.log(id);
			this.loadCart();
		});
	}

	makeTable(tabela) {
		let tab = tabela.map(tabela => {
			return (
				<tr key={tabela.id}>
            <td>
              <span >{tabela.name}</span><br />
              <small>{tabela.description}</small>
            </td>
            <td>{tabela[tabela.size]}</td>
            <td><button type="button" className="btn btn-danger" onClick={() => this.removeFromCart(tabela.id)}>Usuń</button></td>
				</tr>
			);
			
		})
		return tab;
	}

  render() {
    return (
			
    <div onMouseEnter={() => this.loadCart()} className="row">

			<div className="col-lg-12">
				<h2>Koszyk</h2>
				<table className="table">
            <thead>
                  <tr>
                    <th className="col-md-4">Nazwa</th>
                    <th className="col-md-2">Cena</th>
                    <th></th>
                  </tr>
            </thead>
            <tbody>
						{this.state.cart}
            </tbody>
        </table>
				<form>
					<button type="submit" className="btn btn-primary">Złóż zamówienie</button>&nbsp;
					<button type="button" className="btn btn-primary">Wyloguj się</button>	
				</form>
			</div>
		

		</div>
    );
  }
}

export default Cart;