import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import loginPanel from './modalPanel/loginPanel';
import registerPanel from './modalPanel/registerPanel';
import Cart from './modalPanel/cart';
import Menu from './menu';
import {
    BrowserRouter as Router,
    Route, Switch,
    Link
  } from 'react-router-dom';


var online = true; //flaga czy użytkownik jest online

ReactDOM.render(<Menu />,document.getElementById('pizzaTable'));


if(online === false)
{
    ReactDOM.render(
        <a >Zaloguj&nbsp;się&nbsp;| Zarejestruj&nbsp;się</a>
        , document.getElementById('userPanel'));
       
    ReactDOM.render(
        <Router>
            <div>
                <Switch> 
                    <Route exact path="/register" component={registerPanel} />
                    <Route component={loginPanel} />
                </Switch>
            </div>
        </Router>
        
        , document.getElementById('panelLR'));
}
else 
{
    //koszyk 
    ReactDOM.render(
        <Router>
        <div>
            <Switch> 
                <Route component={Cart}/>
            </Switch>
        </div>
        </Router>

    ,document.getElementById('panelLR'));
    
    ReactDOM.render(
          
        <a>Koszyk | Wyloguj się</a>
   
        , document.getElementById('userPanel'));
   
}
