import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import qs from 'qs'
var host = "http://localhost/";

class Menu extends Component {
    constructor(props) {
      super(props)

      this.state = {
        tabela: [],
        pizza: []
      }
    }
    
    componentDidMount() {
      //axios.post('../server/menuloader.php')
      axios.get(host + 'pizza/server/menuloader.php')
      .then( response => {
        console.log('Ajax menu works');
        //console.log(response.data);
        this.setState( (state) => ({tabela: response.data}));
        
        var pizza = this.makeTable(this.state.tabela, "pizza");
        //jeśli chcemy stworzyć nową kategorię wystarczy stworzyć nową tabelę i jej stan
        this.setState( (state) => ({pizza: pizza}));
      }); 

    }

    addToCart(id, size) {
      axios.post(host + 'pizza/server/addtocart.php', qs.stringify({'dish_id': id, 'size': size}))
      .then( response => {
        console.log(qs.stringify({'dish_id': id, 'size': size}));
        console.log('Item add to cart');
      }); 
    }

    makeTable(tabela, menuType) {
      var tab = tabela.map(tabela => {
        if(tabela.type === menuType) {
          return (
            <tr key={tabela.id}>
              <td>
              <span >{tabela.name}</span><br />
              <small>{tabela.description}</small>
            </td>
            <td>{tabela.small ? tabela.small + " zł" : null}</td>
            <td>{tabela.medium ? tabela.medium + " zł" : null}</td>
            <td>{tabela.big ? tabela.big + " zł" : null}</td>
            <td>
              <div className="dropdown">
                  <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dodaj do koszyka &nbsp; 
                  <span className="caret"></span></button>
                  <ul className="dropdown-menu">
                    
                    {tabela.small ?<li onClick={() => this.addToCart(tabela.id, "small")}><a>Mała</a></li> : null}
                    {tabela.medium ?<li onClick={() => this.addToCart(tabela.id, "medium")}><a>Średnia</a></li> : null}
                    {tabela.big ?<li onClick={() => this.addToCart(tabela.id, "big")}><a>Duża</a></li> : null}
                    
                  </ul>
              </div>
            </td>
            </tr>
          );
        }
        else return null;
      });

      return tab;
    }
   
    render() {
        return (
          <table className="table">
            <thead>
                  <tr>
                    <th className="col-md-4">Nazwa</th>
                    <th className="col-md-1">Mała<br />24 cm</th>
                    <th className="col-md-1">Średnia<br />30 cm</th>
                    <th className="col-md-1">Duża<br />43 cm</th>
                    <th></th>
                  </tr>
            </thead>
            <tbody>
              {this.state.pizza}
            </tbody>
          </table>
        );
    }
}

export default Menu;